package org.zzzyxwvut.monetary;

import java.math.MathContext;

/**
 * This interface declares a set of monetary arithmetic operations.
 * <p>
 * These operations observe an instance of {@link java.math.MathContext}
 * either passed as a (binary) method argument or obtained from this type
 * implementation instance passed as a (unary) method argument.
 *
 * @param <T> a monetary calculator type
 */
public interface Calculator<T extends Calculator<T>>
{
	/**
	 * Returns the context used for arithmetic operations.
	 *
	 * @return the context used for arithmetic operations
	 */
	MathContext mathContext();

	/**
	 * Returns a new sum-calculator.
	 *
	 * @param calc an addend-calculator
	 * @param context a numerical context
	 * @return a new sum-calculator of this calculator and
	 *	of the passed one
	 * @throws ArithmeticException if the sum is inexact and
	 *	the rounding mode of the passed numerical context is
	 *	{@code UNNECESSARY}
	 */
	T plus(T calc, MathContext context);

	/**
	 * Returns a new sum-calculator.
	 *
	 * @param calc an addend-calculator
	 * @return a new sum-calculator of this calculator and
	 *	of the passed one
	 * @throws ArithmeticException if the sum is inexact and
	 *	the rounding mode of the obtained numerical context is
	 *	{@code UNNECESSARY}
	 */
	T plus(T calc);

	/**
	 * Returns a new difference-calculator.
	 *
	 * @param calc a subtrahend-calculator
	 * @param context a numerical context
	 * @return a new difference-calculator of this calculator and
	 *	of the passed one
	 * @throws ArithmeticException if the difference is inexact and
	 *	the rounding mode of the passed numerical context is
	 *	{@code UNNECESSARY}
	 */
	T minus(T calc, MathContext context);

	/**
	 * Returns a new difference-calculator.
	 *
	 * @param calc a subtrahend-calculator
	 * @return a new difference-calculator of this calculator and
	 *	of the passed one
	 * @throws ArithmeticException if the sum is inexact and
	 *	the rounding mode of the obtained numerical context is
	 *	{@code UNNECESSARY}
	 */
	T minus(T calc);

	/**
	 * Returns a new product-calculator.
	 *
	 * @param calc a multiplier-calculator
	 * @param context a numerical context
	 * @return a new product-calculator of this calculator and
	 *	of the passed one
	 * @throws ArithmeticException if the product is inexact and
	 *	the rounding mode of the passed numerical context is
	 *	{@code UNNECESSARY}
	 */
	T times(T calc, MathContext context);

	/**
	 * Returns a new product-calculator.
	 *
	 * @param calc a multiplier-calculator
	 * @return a new product-calculator of this calculator and
	 *	of the passed one
	 * @throws ArithmeticException if the sum is inexact and
	 *	the rounding mode of the obtained numerical context is
	 *	{@code UNNECESSARY}
	 */
	T times(T calc);

	/**
	 * Returns a new quotient-calculator.
	 *
	 * @param calc a divisor-calculator
	 * @param context a numerical context
	 * @return a new quotient calculator of this calculator and
	 *	of the passed one
	 * @throws ArithmeticException if the quotient is inexact and
	 *	the rounding mode of the passed numerical context is
	 *	{@code UNNECESSARY} or if the value of the passed
	 *	divisor-calculator equals zero
	 */
	T dividedBy(T calc, MathContext context);

	/**
	 * Returns a new quotient-calculator.
	 *
	 * @param calc a divisor-calculator
	 * @return a new quotient calculator of this calculator and
	 *	of the passed one
	 * @throws ArithmeticException if the quotient is inexact and
	 *	the rounding mode of the obtained numerical context is
	 *	{@code UNNECESSARY} or if the value of the passed
	 *	divisor-calculator equals zero
	 */
	T dividedBy(T calc);
}
