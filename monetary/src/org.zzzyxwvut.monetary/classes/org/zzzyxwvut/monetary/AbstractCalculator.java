package org.zzzyxwvut.monetary;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Comparator;
import java.util.Objects;

/**
 * A skeletal implementation of {@code Calculator}.
 *
 * @param <T> a monetary calculator type
 */
public abstract class AbstractCalculator<T extends AbstractCalculator<T>>
				implements Calculator<T>, Comparable<T>
{
	private static final Comparator<? super AbstractCalculator<?>>
								COMPARATOR =
		Comparator.comparing(AbstractCalculator::monetaryValue);

	private final BigDecimal value;
	private final MathContext context;

	/**
	 * Constructs a new {@code AbstractCalculator} object.
	 *
	 * @param value a monetary value
	 * @param context a numerical context
	 */
	protected AbstractCalculator(BigDecimal value, MathContext context)
	{
		this.value = Objects.requireNonNull(value, "value");
		this.context = Objects.requireNonNull(context, "context");
	}

	/**
	 * Constructs a new {@code AbstractCalculator} object, using
	 * the {@link java.math.MathContext#DECIMAL128} numerical context.
	 *
	 * @param value a monetary value
	 */
	protected AbstractCalculator(BigDecimal value)
	{
		this(value, MathContext.DECIMAL128);
	}

	/**
	 * Creates a new instance of this monetary calculator.
	 *
	 * @param value a monetary value
	 * @param context a numerical context
	 * @return a new instance of this monetary calculator
	 */
	protected abstract T newCalc(BigDecimal value, MathContext context);

	/**
	 * Obtains the monetary value scaled down to match the minor unit
	 * range for this currency.
	 *
	 * @return the monetary value
	 * @throws ArithmeticException if the scaling operation would require
	 *	rounding and the rounding mode is {@code UNNECESSARY}
	 * @see BigDecimal#setScale(int, java.math.RoundingMode)
	 * @see LocalisedMonetaryProperties#minorUnitDigits()
	 */
	protected abstract BigDecimal monetaryValue();

	/**
	 * Returns the raw value.
	 *
	 * @return the raw value
	 */
	protected BigDecimal rawValue()		{ return value; }

	private void assertClassEquivalence(T that)
	{
		if (getClass() != that.getClass())
			throw new IllegalArgumentException(
					"Calculator runtime classes vary");
	}

	@Override
	public MathContext mathContext()	{ return context; }

	@Override
	public T plus(T calc, MathContext context)
	{
		Objects.requireNonNull(calc, "calc");
		Objects.requireNonNull(context, "context");
		assertClassEquivalence(calc);
		return newCalc(rawValue().add(calc.rawValue(), context),
								context);
	}

	@Override
	public T plus(T calc)
	{
		Objects.requireNonNull(calc, "calc");
		assertClassEquivalence(calc);
		return newCalc(rawValue().add(calc.rawValue(),
						calc.mathContext()),
							calc.mathContext());
	}

	@Override
	public T minus(T calc, MathContext context)
	{
		Objects.requireNonNull(calc, "calc");
		Objects.requireNonNull(context, "context");
		assertClassEquivalence(calc);
		return newCalc(rawValue().subtract(calc.rawValue(), context),
								context);
	}

	@Override
	public T minus(T calc)
	{
		Objects.requireNonNull(calc, "calc");
		assertClassEquivalence(calc);
		return newCalc(rawValue().subtract(calc.rawValue(),
						calc.mathContext()),
							calc.mathContext());
	}

	@Override
	public T times(T calc, MathContext context)
	{
		Objects.requireNonNull(calc, "calc");
		Objects.requireNonNull(context, "context");
		assertClassEquivalence(calc);
		return newCalc(rawValue().multiply(calc.rawValue(), context),
								context);
	}

	@Override
	public T times(T calc)
	{
		Objects.requireNonNull(calc, "calc");
		assertClassEquivalence(calc);
		return newCalc(rawValue().multiply(calc.rawValue(),
						calc.mathContext()),
							calc.mathContext());
	}

	@Override
	public T dividedBy(T calc, MathContext context)
	{
		Objects.requireNonNull(calc, "calc");
		Objects.requireNonNull(context, "context");
		assertClassEquivalence(calc);
		return newCalc(rawValue().divide(calc.rawValue(), context),
								context);
	}

	@Override
	public T dividedBy(T calc)
	{
		Objects.requireNonNull(calc, "calc");
		assertClassEquivalence(calc);
		return newCalc(rawValue().divide(calc.rawValue(),
						calc.mathContext()),
							calc.mathContext());
	}

	/**
	 * {@inheritDoc}
	 *
	 * @implSpec
	 * This implementation invokes {@link #monetaryValue()}.
	 */
	@Override
	public int compareTo(T that)
	{
		Objects.requireNonNull(that, "that");
		assertClassEquivalence(that);
		return COMPARATOR.compare(this, that);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @implSpec
	 * This implementation invokes {@link #monetaryValue()}.
	 */
	@Override
	public boolean equals(Object that)
	{
		return (this == that || (that != null
			&& getClass() == that.getClass()
			&& monetaryValue()
				.equals(((AbstractCalculator<?>) that)
							.monetaryValue())));
	}

	/**
	 * {@inheritDoc}
	 *
	 * @implSpec
	 * This implementation invokes {@link #monetaryValue()}.
	 */
	@Override
	public int hashCode()	{ return monetaryValue().hashCode(); }

	/**
	 * {@inheritDoc}
	 *
	 * @implSpec
	 * This implementation invokes {@link #monetaryValue()}.
	 */
	@Override
	public String toString()
	{
		return monetaryValue().toPlainString();
	}
}
