package org.zzzyxwvut.monetary;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.ObjIntConsumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Instances of this class render localised string representation of
 * monetary values numerical in pursuance of the grammar described for
 * {@link java.math.BigDecimal#BigDecimal(String)}.
 */
public final class LocalisedMonetaryStringFilter
{
	private final char decimalSeparator;
	private final char groupingSeparator;
	private final String currencySymbol;

	/**
	 * Constructs a new {@code LocalisedMonetaryStringFilter} object.
	 *
	 * @param properties monetary properties
	 */
	public LocalisedMonetaryStringFilter(MonetaryProperties properties)
	{
		Objects.requireNonNull(properties, "properties");
		decimalSeparator = Objects.requireNonNull(
					properties.decimalSeparator(),
					"decimalSeparator");
		groupingSeparator = Objects.requireNonNull(
					properties.groupingSeparator(),
					"groupingSeparator");
		currencySymbol = Objects.requireNonNull(
					properties.currencySymbol(),
					"currencySymbol");
	}

	private static IntPredicate whitespaceable()
	{
		return Character::isWhitespace;
	}

	private static <T, U> U partialApply(Function<T, U> f, T t)
	{
		return f.apply(t);
	}

	/**
	 * Filters away non-numerical and localised symbols from the passed
	 * value.
	 *
	 * @param value a monetary value
	 * @return the numerical representation of the passed value
	 * @see MonetaryProperties#toLocalisedString(java.math.BigDecimal)
	 */
	public String filter(String value)
	{
		Objects.requireNonNull(value, "value");

		class CodePointIndex
		{
			private final String text;

			private int codePoint = 0;
			private int index = 0;

			CodePointIndex(String text)
			{
				this.text = text;
				nextCodePoint(1);
			}

			boolean hasNextIndex()
			{
				return (index >= 0 && index < text.length());
			}

			private void nextCodePoint(int stride)
			{
				codePoint = (stride == 0)
					? codePoint
					: (hasNextIndex())
						? text.codePointAt(index)
						: 0;		/* '\0' */
			}

			CodePointIndex nextIndex()
			{
				++index;
				nextCodePoint(1);
				return this;
			}

			CodePointIndex nextIndex(int stride)
			{
				index += stride;
				nextCodePoint(stride);
				return this;
			}

			int codePoint()			{ return codePoint; }
			int index()			{ return index; }
		}

		final BiFunction<String, String,
				Function<Integer,
				Predicate<CodePointIndex>>> aheadMatchable =
					(needle, bottle) -> eye -> wisp -> wisp
			.nextIndex(eye == wisp.codePoint()
					&& needle.equals(bottle.substring(
						Math.min(bottle.length(),
								wisp.index()),
						Math.min(bottle.length(),
								wisp.index()
							+ needle.length())))
				? needle.length()
				: 0)
			.hasNextIndex();

		/*
		 * Skip currency symbols (in aheadMatchable) and whitespaces
		 * and monetary grouping separator instances, if any; finally,
		 * substitute "." for a localised monetary decimal separator.
		 */
		return Stream.iterate(new CodePointIndex(value),
				LocalisedMonetaryStringFilter
						.<Predicate<CodePointIndex>,
							Predicate<CodePointIndex>>
								partialApply(
					aheadMatchable_ -> aheadMatchable_::test,
					aheadMatchable
						.apply(currencySymbol, value)
						.apply((currencySymbol.isEmpty())
							? 0
							: currencySymbol
								.codePointAt(0))),
				CodePointIndex::nextIndex)
			.mapToInt(CodePointIndex::codePoint)
			.filter(whitespaceable()
				.or(LocalisedMonetaryStringFilter.<Integer,
							IntPredicate>
								partialApply(
					separator -> codePoint ->
						(codePoint == separator),
					Integer.valueOf(groupingSeparator)))
				.negate())
			.collect(StringBuilder::new,
				LocalisedMonetaryStringFilter.<Integer,
						ObjIntConsumer<StringBuilder>>
								partialApply(
					/*
					 * Consider locales that use "." as
					 * a grouping separator, and filter
					 * such instances before substituting
					 * a decimal separator.
					 */
					separator -> (builder, codePoint) ->
									builder
						.appendCodePoint((codePoint
								== separator)
							? '.'
							: codePoint),
					Integer.valueOf(decimalSeparator)),
				(left, right) -> {
					throw new UnsupportedOperationException();
				})
			.toString();
	}
}
