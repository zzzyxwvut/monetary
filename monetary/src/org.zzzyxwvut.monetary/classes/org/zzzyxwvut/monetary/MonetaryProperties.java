package org.zzzyxwvut.monetary;

import java.math.BigDecimal;

/** This interface declares some monetary properties. */
public interface MonetaryProperties
{
	/**
	 * Returns the number of digits for the minor unit of a currency.
	 *
	 * @return the number of digits for the minor unit of a currency
	 */
	int minorUnitDigits();

	/**
	 * Returns the character used for a decimal separator.
	 *
	 * @return the character used for a decimal separator
	 */
	char decimalSeparator();

	/**
	 * Returns the character used for a grouping separator.
	 *
	 * @return the character used for a grouping separator
	 */
	char groupingSeparator();

	/**
	 * Returns the symbol of the currency for a locale.
	 *
	 * @return the symbol of the currency for a locale
	 */
	String currencySymbol();

	/**
	 * Returns the ISO 4217 currency code for a currency.
	 *
	 * @return the ISO 4217 currency code for a currency
	 */
	String currencyCode();

	/**
	 * Returns the localised string representation of the passed monetary
	 * value.
	 *
	 * @param value a monetary value
	 * @return the localised string representation of the passed monetary
	 *	value
	 */
	String toLocalisedString(BigDecimal value);
}
