package org.zzzyxwvut.monetary;

import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;

/** Locale-tailored {@code MonetaryProperties}. */
public class LocalisedMonetaryProperties implements MonetaryProperties
{
	private final Locale locale;
	private final char decimalSeparator;
	private final char groupingSeparator;
	private final int minorUnitDigits;
	private final String currencySymbol;
	private final String currencyCode;
	private final String displayName;
	private final String format;

	/**
	 * Constructs a new {@code LocalisedMonetaryProperties} object.
	 *
	 * @param locale a locale
	 */
	public LocalisedMonetaryProperties(Locale locale)
	{
		this.locale = Objects.requireNonNull(locale, "locale");
		final DecimalFormatSymbols dfs = DecimalFormatSymbols
							.getInstance(locale);
		decimalSeparator = dfs.getMonetaryDecimalSeparator();
		groupingSeparator = dfs.getGroupingSeparator();
		final Currency currency = Objects.requireNonNull(
							dfs.getCurrency(),
							"currency code");
		minorUnitDigits = currency.getDefaultFractionDigits();
		currencySymbol = currency.getSymbol(locale);
		currencyCode = currency.getCurrencyCode();
		displayName = currency.getDisplayName(locale);
		format = String.format("%s%%,.%df", currencySymbol,
							minorUnitDigits);
	}	/* See java/util/Formatter.html#L10nAlgorithm */

	@Override
	public int minorUnitDigits()		{ return minorUnitDigits; }

	@Override
	public char decimalSeparator()		{ return decimalSeparator; }

	@Override
	public char groupingSeparator()		{ return groupingSeparator; }

	@Override
	public String currencySymbol()		{ return currencySymbol; }

	@Override
	public String currencyCode()		{ return currencyCode; }

	@Override
	public String toLocalisedString(BigDecimal value)
	{
		Objects.requireNonNull(value, "value");
		return String.format(locale, format, value);
	}

	@Override
	public String toString()		{ return displayName; }
}
