/** Defines monetary support. */
module org.zzzyxwvut.monetary
{
	requires static org.zzzyxwvut.ate;

	exports org.zzzyxwvut.monetary;
}
