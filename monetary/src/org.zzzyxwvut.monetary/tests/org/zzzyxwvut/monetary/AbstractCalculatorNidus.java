package org.zzzyxwvut.monetary;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.Instantiable;

class AbstractCalculatorNidus
{
	static <T extends Testable> Function<Set<Supplier<? extends T>>,
						Instantiable<T>> builder()
	{
		return testables -> Instantiable.<T>newBuilder(testables)
			.reportable(Result.resultor()
				.apply(Result.renderer()
					.apply(Map.of(OtherResult.class,
						OtherResult.RENDITION_PLAIN,
						TestResult.class,
						TestResult.RENDITION_REVERSE_VIDEO))))
			.build();
	}

	public static class ὈβολόςCalcTests implements Testable
	{
		public void testPlusWith11and7()
		{
			final ὈβολόςCalc expected = ὈβολόςCalc.from("18");
			final ὈβολόςCalc obtained = ὈβολόςCalc.from("11")
				.plus(ὈβολόςCalc.from("7"));
			assert expected.equals(obtained) : obtained.toString();
		}

		public void testName()
		{
			final String expected = "ὀβολοί";
			final String obtained = ὈβολόςCalc.from("3").name();
			assert expected.equals(obtained) : obtained;
		}

		public void testCompareToConsistenceWithEquals()
		{
			final ὈβολόςCalc ὀβολοίX0 = ὈβολόςCalc.from("1.00");
			final ὈβολόςCalc ὀβολοίX1 = ὈβολόςCalc.from("1");
			assert (ὀβολοίX0.compareTo(ὀβολοίX1) == 0)
						&& ὀβολοίX0.equals(ὀβολοίX1);
		}

		public static Instantiable<ὈβολόςCalcTests> instantiable()
		{
			return AbstractCalculatorNidus
						.<ὈβολόςCalcTests>builder()
				.apply(Set.of(ὈβολόςCalcTests::new));
		}
	}

	public static class AsCalcTests implements Testable
	{
		public void testPlusWith2and9()
		{
			final AsCalc expected = AsCalc.from("11");
			final AsCalc obtained = AsCalc.from("2")
				.plus(AsCalc.from("9"));
			assert expected.equals(obtained) : obtained.toString();
		}

		public void testName()
		{
			final String expected = "asses";
			final String obtained = AsCalc.from("2").name();
			assert expected.equals(obtained) : obtained;
		}

		public void testCompareToConsistenceWithEquals()
		{
			final AsCalc assesX0 = AsCalc.from("1.00");
			final AsCalc assesX1 = AsCalc.from("1");
			assert (assesX0.compareTo(assesX1) == 0)
						&& assesX0.equals(assesX1);
		}

		public static Instantiable<AsCalcTests> instantiable()
		{
			return AbstractCalculatorNidus.<AsCalcTests>builder()
				.apply(Set.of(AsCalcTests::new));
		}
	}

	public static class MixingCalcTests implements Testable
	{
		public void testPlusWithDisparateTypes()
		{
			final Class<?> klass = AbstractCalculator.class;
			final MethodType plusMT = MethodType.methodType(klass,
									klass);
			final AbstractCalculator<?> ὀβολοί = ὈβολόςCalc
								.from("10");
			final AbstractCalculator<?> asses = AsCalc.from("11");
			ὈβολόςCalc obtained = null;

			try {
				final MethodHandle mh = MethodHandles
					.publicLookup()
					.in(klass)
					.findVirtual(klass, "plus", plusMT);
				obtained = (ὈβολόςCalc) mh.invoke(ὀβολοί, asses);
			} catch (final Throwable e) {
				assert (e instanceof IllegalArgumentException)
					&& "Calculator runtime classes vary"
						.equals(e.getMessage()) :
							e.getMessage();
				return;
			}

			throw new AssertionError();
		}

		public void testCompareToWithDisparateTypes()
		{
			final Class<?> klass = AbstractCalculator.class;
			final MethodType compareToMH = MethodType.methodType(
								int.class,
								klass);

			final AbstractCalculator<?> ὀβολοί = ὈβολόςCalc
								.from("10");
			final AbstractCalculator<?> asses = AsCalc.from("10");
			Integer obtained = null;

			try {
				final MethodHandle mh = MethodHandles
					.publicLookup()
					.in(klass)
					.findVirtual(klass, "compareTo",
								compareToMH);
				obtained = (int) mh.invoke(ὀβολοί, asses);
			} catch (final Throwable e) {
				assert (e instanceof IllegalArgumentException)
					&& "Calculator runtime classes vary"
						.equals(e.getMessage()) :
							e.getMessage();
				return;
			}

			throw new AssertionError();
		}

		public void testEqualsWithDisparateTypes()
		{
			final AbstractCalculator<?> ὀβολοί = ὈβολόςCalc
								.from("10");
			final AbstractCalculator<?> asses = AsCalc.from("10");
			assert !ὀβολοί.equals(asses) : "disparate types";
		}

		public static Instantiable<MixingCalcTests> instantiable()
		{
			return AbstractCalculatorNidus
						.<MixingCalcTests>builder()
				.apply(Set.of(MixingCalcTests::new));
		}
	}

	static class ProxiedMonetaryProperties
	{
		static Function<Object[], Object> minorUnitDigits()
		{
			return args -> 0;
		}

		static Function<Object[], Object> toLocalisedString()
		{
			return args -> {
				if (Objects.requireNonNull(args[0], "value")
								.getClass()
							!= BigDecimal.class)
					throw new IllegalArgumentException(
						"Expected BigDecimal argument");

				final BigDecimal value = (BigDecimal) args[0];
				return String.format("%.0f", value);
			};
		}
	}

	static class ὈβολόςCalc extends AbstractCalculator<ὈβολόςCalc>
	{
		private static final MonetaryProperties PROPERTIES =
				(MonetaryProperties) Proxy.newProxyInstance(
			MonetaryProperties.class.getClassLoader(),
			new Class<?>[] { MonetaryProperties.class },
			new ProxyHandler(Map.of(
				"minorUnitDigits",
					ProxiedMonetaryProperties
							.minorUnitDigits(),
				"toLocalisedString",
					ProxiedMonetaryProperties
							.toLocalisedString(),
				"toString",	/* 1 δραχμή == 6 ὀβολοί. */
					args -> "ὀβολοί")));

		/* Assume translation from Α, Β, &c. to BigDecimal. */
		private ὈβολόςCalc(BigDecimal value, MathContext context)
		{
			super(value, context);
		}

		static ὈβολόςCalc from(String value, MathContext context)
		{
			Objects.requireNonNull(value, "value");
			Objects.requireNonNull(context, "context");
			return new ὈβολόςCalc(new BigDecimal(value), context);
		}

		static ὈβολόςCalc from(String value)
		{
			return from(value, MathContext.DECIMAL128);
		}

		@Override
		protected ὈβολόςCalc newCalc(BigDecimal value,
							MathContext context)
		{
			return new ὈβολόςCalc(value, context);
		}

		@Override
		protected BigDecimal monetaryValue()
		{
			return rawValue()
				.setScale(PROPERTIES.minorUnitDigits(),
					mathContext().getRoundingMode());
		}

		String toLocalisedString()
		{
			return PROPERTIES.toLocalisedString(monetaryValue());
		}

		String name()		{ return PROPERTIES.toString(); }

		@Override
		public String toString()
		{
			return monetaryValue().toPlainString();
		}
	}

	static class AsCalc extends AbstractCalculator<AsCalc>
	{
		private static final MonetaryProperties PROPERTIES =
				(MonetaryProperties) Proxy.newProxyInstance(
			MonetaryProperties.class.getClassLoader(),
			new Class<?>[] { MonetaryProperties.class },
			new ProxyHandler(Map.of(
				"minorUnitDigits",
					ProxiedMonetaryProperties
							.minorUnitDigits(),
				"toLocalisedString",
					ProxiedMonetaryProperties
							.toLocalisedString(),
				"toString",	/* 1 denarius == 10 asses. */
					args -> "asses")));

		/* Assume translation from I, II, &c. to BigDecimal. */
		private AsCalc(BigDecimal value, MathContext context)
		{
			super(value, context);
		}

		static AsCalc from(String value, MathContext context)
		{
			Objects.requireNonNull(value, "value");
			Objects.requireNonNull(context, "context");
			return new AsCalc(new BigDecimal(value), context);
		}

		static AsCalc from(String value)
		{
			return from(value, MathContext.DECIMAL128);
		}

		@Override
		protected AsCalc newCalc(BigDecimal value, MathContext context)
		{
			return new AsCalc(value, context);
		}

		@Override
		protected BigDecimal monetaryValue()
		{
			return rawValue()
				.setScale(PROPERTIES.minorUnitDigits(),
					mathContext().getRoundingMode());
		}

		String toLocalisedString()
		{
			return PROPERTIES.toLocalisedString(monetaryValue());
		}

		String name()		{ return PROPERTIES.toString(); }

		@Override
		public String toString()
		{
			return monetaryValue().toPlainString();
		}
	}
}
