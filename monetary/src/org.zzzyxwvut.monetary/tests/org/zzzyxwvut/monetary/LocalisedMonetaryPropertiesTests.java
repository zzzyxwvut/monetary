package org.zzzyxwvut.monetary;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Locale;

import org.zzzyxwvut.ate.Testable;

@SuppressWarnings({ "exports", "missing-explicit-ctor" })
public class LocalisedMonetaryPropertiesTests implements Testable
{
	private static final MathContext MATH_CONTEXT =
						MathContext.DECIMAL128;
	private static final MonetaryProperties PROPERTIES =
				new LocalisedMonetaryProperties(Locale.US);

	public void testToLocalisedStringWithNull()
	{
		String obtained = null;

		try {
			obtained = PROPERTIES.toLocalisedString(null);
		} catch (final NullPointerException ignored) {
		}

		assert obtained == null : "<null>";
	}

	public void testToLocalisedStringWithNegativeChange()
	{
		final String expected = "$-0.10";
		final String obtained = PROPERTIES.toLocalisedString(
				new BigDecimal("-0.1", MATH_CONTEXT));
		assert expected.equals(obtained) : obtained;
	}

	public void testToLocalisedStringWithZero()
	{
		final String expected = "$0.00";
		final String obtained = PROPERTIES.toLocalisedString(
				new BigDecimal("0", MATH_CONTEXT));
		assert expected.equals(obtained) : obtained;
	}

	public void testToLocalisedStringWithPositiveChange()
	{
		final String expected = "$0.10";
		final String obtained = PROPERTIES.toLocalisedString(
				new BigDecimal("+0.1", MATH_CONTEXT));
		assert expected.equals(obtained) : obtained;
	}

	public void testToLocalisedStringWithMillionAndChange()
	{
		final String expected = "$1,234,567.89";
		final String obtained = PROPERTIES.toLocalisedString(
				new BigDecimal("1234567.89", MATH_CONTEXT));
		assert expected.equals(obtained) : obtained;
	}

	public void testToLocalisedStringWithMillionAndChangeInGermanLocale()
	{
		final String expected = "€1.234.567,89";
		final String obtained = new LocalisedMonetaryProperties(
							Locale.GERMANY)
			.toLocalisedString(new BigDecimal("1234567.89",
							MATH_CONTEXT));
		assert expected.equals(obtained) : obtained;
	}
}
