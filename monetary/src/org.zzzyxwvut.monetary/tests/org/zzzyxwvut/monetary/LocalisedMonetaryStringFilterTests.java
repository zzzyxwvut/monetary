package org.zzzyxwvut.monetary;

import java.util.Locale;

import org.zzzyxwvut.ate.Testable;

@SuppressWarnings({ "exports", "missing-explicit-ctor" })
public class LocalisedMonetaryStringFilterTests implements Testable
{
	private static final LocalisedMonetaryStringFilter LMSF =
			new LocalisedMonetaryStringFilter(
				new LocalisedMonetaryProperties(Locale.US));

	public void testFilterWithNull()
	{
		String obtained = null;

		try {
			obtained = LMSF.filter(null);
		} catch (final NullPointerException ignored) {
		}

		assert obtained == null : "<null>";
	}

	public void testFilterWithEmptyValue()
	{
		final String expected = "";
		final String obtained = LMSF.filter("");
		assert expected.equals(obtained) : obtained;
	}

	public void testFilterWithCurrencySymbolOnly()
	{
		final String expected = "";
		final String obtained = LMSF.filter("$");
		assert expected.equals(obtained) : obtained;
	}

	public void testFilterWithNegativeChange()
	{
		final String expected = "-0.1";
		final String obtained = LMSF.filter("$-0.1");
		assert expected.equals(obtained) : obtained;
	}

	public void testFilterWithTrailingSymbol()
	{
		final String expected = "0";
		final String obtained = LMSF.filter("0$");
		assert expected.equals(obtained) : obtained;
	}

	public void testFilterWithIntercalatedSymbol()
	{
		final String expected = "00";
		final String obtained = LMSF.filter("0$0");
		assert expected.equals(obtained) : obtained;
	}

	public void testFilterWithLeadingSymbol()
	{
		final String expected = "0";
		final String obtained = LMSF.filter("$0");
		assert expected.equals(obtained) : obtained;
	}

	public void testFilterWithMillionAndChange()
	{
		final String expected = "1234567.89";
		final String obtained = LMSF.filter("   $1,234,567.89");
		assert expected.equals(obtained) : obtained;
	}

	public void testFilterWithMillionAndChangeInGermanLocale()
	{
		final String expected = "1234567.89";
		final String obtained = new LocalisedMonetaryStringFilter(
				new LocalisedMonetaryProperties(
							Locale.GERMANY))
			.filter("   €1.234.567,89");
		assert expected.equals(obtained) : obtained;
	}
}
