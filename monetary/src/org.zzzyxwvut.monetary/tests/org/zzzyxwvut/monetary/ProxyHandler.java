package org.zzzyxwvut.monetary;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

/** A general-purpose {@code InvocationHandler}. */
class ProxyHandler implements InvocationHandler
{
	private final Map<String, Function<Object[], Object>> methods;

	/**
	 * Constructs a new {@code ProxyHandler} object.
	 *
	 * @param methods a map of implemented methods
	 */
	ProxyHandler(Map<String, Function<Object[], Object>> methods)
	{
		this.methods = Objects.requireNonNull(methods, "methods");
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
							throws Throwable
	{
		if (!Proxy.isProxyClass(proxy.getClass()))
			throw new IllegalArgumentException(
						"Not a proxy class");

		return Optional.ofNullable(methods.get(method.getName()))
			.orElseThrow(UnsupportedOperationException::new)
			.apply(args);
	}
}
