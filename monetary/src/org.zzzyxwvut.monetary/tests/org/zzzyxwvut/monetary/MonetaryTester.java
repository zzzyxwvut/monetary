package org.zzzyxwvut.monetary;

import java.util.Set;

import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.Tester;

class MonetaryTester
{
	static {
		MonetaryTester.class
			.getClassLoader()
			.setPackageAssertionStatus("org.zzzyxwvut.monetary",
									true);
	}

	private MonetaryTester() { /* No instantiation. */ }

	public static void main(String... args)
	{
		System.exit((Tester.newBuilder(Set.of(
				AbstractCalculatorNidus.AsCalcTests.class,
				AbstractCalculatorNidus.MixingCalcTests.class,
				AbstractCalculatorNidus.ὈβολόςCalcTests.class,
				LocalisedMonetaryPropertiesTests.class,
				LocalisedMonetaryStringFilterTests.class))
			.executionPolicy(Testable.ExecutionPolicy.CONCURRENT)
			.verifyAssertion()
			.build()
			.runAndReport()) ? 0 : 1);
	}
}
