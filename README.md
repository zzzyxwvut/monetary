Monetary support.

Take these steps to configure this project's build:

* supply an Apache Ivy library on the class path  
(see <https://ant.apache.org/ivy/download.cgi>)  
(see <https://ant.apache.org/manual/install.html#optionalTasks>)

* adapt the path to a local (to be) Apache Ivy repository for publishing  
(use `monetary.ivy.repo.ivy2.dir` in `monetary/common/ivy.properties`)

* adapt the path to a local (to be) Apache Maven repository for publishing  
(use `monetary.ivy.repo.m2.dir` in `monetary/common/ivy.properties`)

* adapt the path to a JDK-17 (or newer) compiler for the `ant` `javac` task  
(use `test.javac.executable` in `monetary/common/build.properties`)

This project depends on `ate` available from
[here](https://bitbucket.org/zzzyxwvut/ate.git "here").
Before building this project, make a local installation of `ate`.

Build the project and locally publish its Apache Ivy and Apache Maven
artifacts:  
`cd repos/monetary/monetary/`  
`ant -projecthelp`  
`ant ivy-publish ## -lib ${IVYJAR:?}`
